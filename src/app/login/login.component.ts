
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SimpleCrypt } from 'ngx-simple-crypt';
import { DataService } from '../data.service';
import { ToastrService } from 'ngx-toastr';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public loginReq = {
      'email' : '',
      'password' : '',
    };
    public hide : any = true ;
    public loginForm : FormGroup;
    public loading : any = false ;
    public submitted : any = false ;
  constructor(private formBuilder: FormBuilder,private data : DataService,private http : HttpClient,private toastr: ToastrService) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required,Validators.email]],
      password: ['',Validators.required],
    });
  }
  login(){
    this.submitted = true ;
// Begin email and password fields validation
  if(this.loginReq.email != "" && this.loginReq.password != "" && this.loginForm.valid){
    this.loading = true ;
    // let baseUrl = "http://lodge.innsandbox.com/api/user/login";
    this.http.post('user/login', this.loginReq).subscribe(response => {
      if(response['status'].code == "200"){
        this.loading = false ;
        this.toastr.success(response['status'].message);
        let simpleCrypt = new SimpleCrypt();
        let encodedToken = simpleCrypt.encode("INNOVx123", response['results'].token);
        localStorage.setItem("User", encodedToken);
        localStorage.setItem("userDetails", JSON.stringify(response['results'].user));
        this.data.setUser(response['results'].user);
        document.getElementById("closerX20").click();
      }
      else { this.toastr.error(response['status'].message);
      this.loading = false ;
     }
    }, error => {
      alert("Something wrong happened !");
  });
  } // End email and password fields validation
  else { this.toastr.error('Please enter email and password'); }
} // End login function
get f(){
  return this.loginForm.controls;
}
}

