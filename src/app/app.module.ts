import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxPopper } from 'angular-popper';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { FlickityModule } from 'ngx-flickity';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxCaptchaModule } from 'ngx-captcha';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';
import { AnnouncementComponent } from './announcement/announcement.component';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { InterceptService } from './intercept.service';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';

import { FormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { PopupsubmitComponent } from './popupsubmit/popupsubmit.component';
import { PopupresetpwComponent } from './popupresetpw/popupresetpw.component';
import { GrantsComponent } from './grants/grants.component';
import { FaqComponent } from './faq/faq.component';


const routes: Routes = [
{ path: 'header', component: HeaderComponent },
{path:'footer',component:FooterComponent},
{path:'',component:HomeComponent},
{path:'announcement/:id',component:AnnouncementComponent},
{path:'grants',component:GrantsComponent},
{path:'faq/:id',component:FaqComponent},

];
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    FooterComponent,
    AnnouncementComponent,
    SignupComponent,
    LoginComponent,
    PopupsubmitComponent,
    PopupresetpwComponent,
    GrantsComponent,
    FaqComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    NgbModule,
    NgxPopper,
    MDBBootstrapModule.forRoot(),
    FlickityModule,
    SlickCarouselModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ToastrModule.forRoot(), // ToastrModule added
    ReactiveFormsModule,
    NgxCaptchaModule,

  ],
  providers: [{ provide: HTTP_INTERCEPTORS , useClass: InterceptService,multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
