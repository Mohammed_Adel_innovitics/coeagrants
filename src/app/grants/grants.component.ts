import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule, FormBuilder, Validators, FormGroup } from '@angular/forms';
import { NgxCaptchaModule } from 'ngx-captcha';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-grants',
  templateUrl: './grants.component.html',
  styleUrls: ['./grants.component.css']
})
export class GrantsComponent implements OnInit {
public key : any ;
public objGrant : any ;
public objProgram: any;
public applyReq : any = {
  'personal_info' : {},
  'email': "",
  'phone': "",
  'title': "",
  'prsubcategory': "",
  'duration': "",
  // 'date_submitted': "",
  'pi_name': "",
  'pi_university': "",
  'university_position': "",
  'sign_name': "",
  'sign_id': ""
  // 'grant_id': "",
};
public docsUrl : any = [] ;
public docsPusher = [];
public docsForm = new FormData();
public docsPusherName = [];

    public readonly siteKey = '6LfO9MMUAAAAAAyEaZ9ETnQJ-DZkx0fakCEyVld7';
//6LcvoUgUAAAAAJJbhcXvLn3KgG-pyULLusaU4mL1//test
//6Lehjr0UAAAAABCQc_dCLVAJRVCwvKnaLLxA05QU//prod
//grant domain 6LfO9MMUAAAAAAyEaZ9ETnQJ-DZkx0fakCEyVld7
    public captchaIsLoaded = false;
    public captchaSuccess = false;
    public captchaIsExpired = false;
    public captchaResponse?: string;

    public theme: 'light' | 'dark' = 'light';
    public size: 'compact' | 'normal' = 'normal';
    public lang = 'en';
    public type: 'image' | 'audio';

    public langInput: any = false;
    public captchaElem: any = false;

    public hljs: any;

    public useGlobalDomain: boolean = false;

    public recaptchaValue = false;

    public aFormGroup1:FormGroup;

    public disabledAgreement: boolean = false;
    public applyFlag : any = false ;
    public photosCounter : any = 0;
    public loading : any = false ;
    public submitted : any = false ;
    public prcategory: any = '';
    public prsubcategory: any = {};
    public arrCategories: any = [];
    public arrSubCategories: any = [];

  constructor(private routerr : Router ,public toastr: ToastrService,private http : HttpClient,private route: ActivatedRoute,private formBuilder: FormBuilder, private cdr: ChangeDetectorRef) { }

  ngOnInit() {
//     this.key = this.route.snapshot.paramMap.get('id');
//     this.http.post('application/check',{'program_id' : this.key }).subscribe(response => {
//     if(response['status'].code == "200"){
//             this.getProgram();
          if(localStorage.getItem('User') == null){
            this.routerr.navigate(['../']);
          }else{
                  this.getGrant();
                  this.prCategoryGet();
          }
//     }else{
//       this.toastr.error(response['status'].message);
//     }
// });
  }

  ngAfterViewInit(): void {
    this.highlight();
  }

  handleReset(): void {
    this.captchaSuccess = false;
    this.captchaResponse = undefined;
    this.captchaIsExpired = false;
    this.cdr.detectChanges();
  }

  handleSuccess(captchaResponse: string): void {
    this.captchaSuccess = true;
    this.captchaResponse = captchaResponse;
    this.captchaIsExpired = false;
    this.cdr.detectChanges();
    this.recaptchaValue = true;
    console.log('success');
  }

  handleLoad(): void {
    this.captchaIsLoaded = true;
    this.captchaIsExpired = false;
    this.cdr.detectChanges();
  }

  handleExpire(): void {
    this.captchaSuccess = false;
    this.captchaIsExpired = true;
    this.cdr.detectChanges();
  }

  changeTheme(theme: 'light' | 'dark'): void {
    this.theme = theme;
  }

  changeSize(size: 'compact' | 'normal'): void {
    this.size = size;
  }

  changeType(type: 'image' | 'audio'): void {
    this.type = type;
  }

  setLanguage(): void {
    this.lang = this.langInput.nativeElement.value;
  }

  setUseGlobalDomain(use: boolean): void {
    this.useGlobalDomain = use;
  }

  getCurrentResponse(): void {
    const currentResponse = this.captchaElem.getCurrentResponse();
    if (!currentResponse) {
      alert('There is no current response - have you submitted captcha?');
    } else {
      console.log(currentResponse);
    }
  }

  getResponse(): void {
    const response = this.captchaElem.getResponse();
    if (!response) {
      alert('There is no response - have you submitted captcha?');
    } else {
      console.log(response);
    }
  }

  reload(): void {
    this.captchaElem.reloadCaptcha();
  }

  getCaptchaId(): void {
    alert(this.captchaElem.getCaptchaId());
  }

  reset(): void {
    this.captchaElem.resetCaptcha();
  }

  private highlight(): void {
    const highlightBlocks = document.getElementsByTagName('code');
    for (let i = 0; i < highlightBlocks.length; i++) {
      const block = highlightBlocks[i];
      this.hljs.highlightBlock(block);
    }
  }

  getGrant(){
    // this.http.post('programs/get',{'program_id' : this.key }).subscribe(response => {
  //  if(response['status'].code == "200"){
      this.objGrant = JSON.parse(localStorage.getItem('grant'));
      this.applyReq['grant_id'] = this.objGrant.id;
      // if(this.objProgram.form_id == 1){
      this.aFormGroup1 = this.formBuilder.group({
          recaptcha: ['', Validators.required],
          Family_Name : ['', Validators.required],
          First_Name : ['', Validators.required],
          Middle_Name : ['', Validators.required],
          Date_Of_Birth : ['', Validators.required],
          Place_Of_Birth : [''],
          Gender : [''],
          email: ['', Validators.required],
          phone: ['', Validators.required],
          title: ['', Validators.required],
          prcategory: ['', Validators.required],
          prsubcategory: ['', Validators.required],
          duration: ['', Validators.required],
          // date_submitted: ['', Validators.required],
          pi_name: ['', Validators.required],
          pi_university: ['', Validators.required],
          university_position: ['', Validators.required],
          project_participants: [''],
          pi_cvs: ['', Validators.required],
          team_cvs: [''],
          concept_noted: ['', Validators.required],
          disable : ['', Validators.required],
          signName : ['', Validators.required],
          signID : ['', Validators.required],
        });
      // }
     // }

  //	});
  }


  docsUploadFunc(event,filename){

   let elem :any = event.target;  //line 2
   if(elem.files.length > 0){
     let file = new FileReader();
     file.readAsDataURL(elem.files[0]);
     this.docsPusher[filename]= elem.files[0];
       file.onload = (_event) => {
      }
    }
    elem.value = ""; //line 9
    }
    changeCheck(event){
    this.disabledAgreement = !this.disabledAgreement;
    if(this.disabledAgreement == true){
      this.applyFlag = true ;
    }else{
      this.applyFlag = false ;
    }
  }
  changeRecaptcha(){
  this.recaptchaValue = true ;
}

get f() {
    return this.aFormGroup1.controls;
 }

 uploadPhotos(){
   this.submitted = true ;
     if(this.aFormGroup1.invalid){
       this.toastr.error('please upload all photos and required fields');
     return;
     }

   //check//
   this.loading = true ;
   this.http.post('grantapp/check',{'grant_id' : this.objGrant.id }).subscribe(response => {
if(response['status'].code == "200"){

   ////check//
   var filenames = [];
   filenames = Object.keys(this.docsPusher);
     for(let i = 0 ; i < filenames.length ; i ++){
       this.docsForm.append('image', this.docsPusher[filenames[i]]);
       this.docsForm.append('grant_id', this.objGrant.id);
       this.docsForm.append('type', filenames[i]);
       this.http.post('grantapp/docs/upload', this.docsForm).subscribe(response => {
           if(response['status'].code == "200"){
               // this.toastr.success(response['status'].message);
               this.photosCounter = this.photosCounter + 1 ;
           }
           else {
             this.toastr.error(response['status'].message);
             this.loading = false;
          }

         }, error => {

           this.toastr.error(error['error'].message);
           this.loading = false;
         });
     }
     this.apply();
 }else{
     this.toastr.error(response['status'].message);
     }
 });
 }
 apply(){
   if(this.photosCounter != this.docsPusher.length){
     return
   }else{
   this.http.post('grantapp/apply',this.applyReq).subscribe(response => {
   if(response['status'].code == "200"){
         this.toastr.success("Submitted Successfully");
         this.loading = false;
         this.routerr.navigate(['../'])
   }else{
     console.log(this.aFormGroup1.controls);
     this.toastr.error(response['status'].message);
   }
    });
  }
}

// Function to get research area 1

prCategoryGet(){

    this.http.get('grantapp/categories').subscribe(response => {

        if(response['status'].code == "200"){

          this.arrCategories = response['result'];
          console.log(this.arrCategories);


        }

        else { this.toastr.error(response['status'].message); }

      }, error => {

        this.toastr.error(error['error'].message);

      });

}

// End get research area 1


// Function to get research area 2

prsubCategoryGet(){

    let requester = {'cat_id': this.prcategory};

    this.http.post('grantapp/subcategories', requester).subscribe(response => {

        if(response['status'].code == "200"){


          this.arrSubCategories = response['result'];


        }

        else { this.toastr.error(response['status'].message); }

      }, error => {

        this.toastr.error(error['error'].message);

      });

}

// End get research area 2
handleError(){}
}
