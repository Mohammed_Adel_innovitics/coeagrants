import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { DataService } from '../data.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-announcement',
  templateUrl: './announcement.component.html',
  styleUrls: ['./announcement.component.css']
})
export class AnnouncementComponent implements OnInit {
public key : any ;
public objGrant : any ;
public textLodaing : any = true;
public user : any ;
public loading : any = false ;
  constructor(public toastr: ToastrService,private data : DataService,private router : Router,private http : HttpClient,private route: ActivatedRoute) { }

  ngOnInit() {
    this.key = this.route.snapshot.paramMap.get('id');
    this.getGrant();
    this.data.User.subscribe(userData => this.user = userData);
  }
  getGrant(){
    this.http.post('grants/get',{'grant_id' : this.key }).subscribe(response => {
    if(response['status'].code == "200"){
      this.textLodaing = false;
      this.objGrant = response['result'][0];
    }
  });
  }
  applyNow(){
      if(this.user != null){
        this.loading = true;
        this.http.post('grantapp/check',{'grant_id' : this.key }).subscribe(response => {
        if(response['status'].code == "200"){
          localStorage.setItem('grant',JSON.stringify(this.objGrant));
        this.router.navigate(['../grants']);
      }else{
          this.toastr.error(response['status'].message);
          }
          this.loading = false;
    });
      }else{
        document.getElementById('signupAnchor').click();
      }
  }
}
