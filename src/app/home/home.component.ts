import { Component, OnInit } from '@angular/core';
import { NgxPopper } from 'angular-popper';
import { FlickityModule } from 'ngx-flickity';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public arrGrants : any ;
  public textLodaing : any = true;
  constructor(private http:HttpClient) { }

  ngOnInit() {
    this.listGrants();
  }

  images = "assets/1.jpg";

  slides = [
    {img: "assets/partner-1.png"},
    {img: "assets/partner-2.png"},
    {img: "assets/partner-3.png"},
    {img: "assets/partner-4.png"},
    {img: "assets/partner-5.png"},
    {img: "assets/partner-6.png"},
  ];

  slideConfig = {"slidesToShow": 4, "slidesToScroll": 4, "dots":true, "nextArrow":"<div class='nav-btn next-slide'></div>",
    "prevArrow":"<div class='nav-btn prev-slide'></div>",};

  addSlide() {
    this.slides.push({img: "http://placehold.it/350x150/777777"})
  }

  removeSlide() {
    this.slides.length = this.slides.length - 1;
  }

  slickInit(e) {
    console.log('slick initialized');
  }

  breakpoint(e) {
    console.log('breakpoint');
  }

  afterChange(e) {
    console.log('afterChange');
  }

  beforeChange(e) {
    console.log('beforeChange');
  }
  listGrants(){
  this.http.get('grants/list').subscribe(response => {
    if(response['status'].code == "200"){
      this.textLodaing = false;
      this.arrGrants = response['result'];
      this.arrGrants.map(obj=>{
        if(new Date() < new Date(obj.deadline)){
          obj.status = 'Submission Open';
        }else{
          obj.status = 'Submission Closed';
        }
      })
    }else{
    }
  });

}

}
