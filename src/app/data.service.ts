import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor() { }
  private user = new BehaviorSubject(JSON.parse(localStorage.getItem('userDetails')));
    User = this.user.asObservable();

  setUser(User: any) {
    this.user.next(User);
  }
}
