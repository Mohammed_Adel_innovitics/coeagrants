import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupresetpwComponent } from './popupresetpw.component';

describe('PopupresetpwComponent', () => {
  let component: PopupresetpwComponent;
  let fixture: ComponentFixture<PopupresetpwComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupresetpwComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupresetpwComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
