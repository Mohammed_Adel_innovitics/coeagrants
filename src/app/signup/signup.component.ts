import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { HttpClient } from '@angular/common/http';
import { SimpleCrypt } from 'ngx-simple-crypt';
import { DataService } from '../data.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
public signupReq : any = {
  'name' : '',
  'email' : '',
  'password' : '',
}
public hide : any = true ;
public signupForm : FormGroup;
public loading : any = false ;
public submitted : any = false;
  constructor(private formBuilder: FormBuilder,private data : DataService,private http : HttpClient,private toastr: ToastrService) { }

  ngOnInit() {
    this.signupForm = this.formBuilder.group({
      email: ['', [Validators.required,Validators.email]],
      password: ['',Validators.required],
      name: ['',[Validators.required,Validators.maxLength(50)]],
    });
  }
  register(){
    this.submitted = true
    // Begin email and password fields validation
        if(this.signupForm.valid && this.signupReq.email != "" && this.signupReq.password != "" && this.signupReq.name != "" && this.signupReq.id_number != ""){
          this.loading = true ;
          // let checkUrl = "http://lodge.innsandbox.com/api/user/checkAccount";
          this.http.post('user/register', this.signupReq).subscribe(response => {
            // Check of the response from the check is 200
            if(response['status'].code == "200"){

              this.toastr.success(response['status'].message);
              document.getElementById("closer").click();
              let simpleCrypt = new SimpleCrypt();
              let encodedToken = simpleCrypt.encode("INNOVx123", response['results'].token);
              localStorage.setItem("User", encodedToken);
              localStorage.setItem("userDetails", JSON.stringify(response['results'].user));
              this.data.setUser(response['results'].user);
            } // Check that the response of the check is 200
            else { this.toastr.error(response['status'].message);
            }
            this.loading = false;
          });
        } // End email and password fields validation
        else { this.toastr.error('Please enter all fields'); }
  }
  get f(){
    return this.signupForm.controls;
  }
}
