import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupsubmitComponent } from './popupsubmit.component';

describe('PopupsubmitComponent', () => {
  let component: PopupsubmitComponent;
  let fixture: ComponentFixture<PopupsubmitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupsubmitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupsubmitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
