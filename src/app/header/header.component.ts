import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule, Routes } from '@angular/router';
import { DataService } from '../data.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
public user : any ;
  constructor(private data : DataService,private titleService: Title) { }

  isNavbarCollapsed =  false;

  ngOnInit() {
    this.data.User.subscribe(userData => this.user = userData);


  }
  logout(){

    localStorage.clear();
    location.reload();

  }

}
