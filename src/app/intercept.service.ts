import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse
} from '@angular/common/http';
import { Observable } from 'rxjs';
import {SimpleCrypt} from "ngx-simple-crypt";
import { environment } from 'src/environments/environment';
@Injectable()
export class InterceptService implements HttpInterceptor {
public token : any;
  constructor() { }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if(localStorage.getItem('User') == null){
                    req = req.clone({
                    url: `${environment.apiUrl}/${req.url}`,
                    setHeaders: {
                    }
                  });

}else{
      let objCrypt = new SimpleCrypt();
      const token: string = objCrypt.decode('INNOVx123',localStorage.getItem('User'));
                req = req.clone({
                url: `${environment.apiUrl}/${req.url}`,
                setHeaders: {
                            Authorization: `Bearer `+token

                }
                });

}
              return next.handle(req);
}

}
