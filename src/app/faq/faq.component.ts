import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { DataService } from '../data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.css']
})
export class FaqComponent implements OnInit {
  public key : any ;
  public user : any ;
  public textLodaing : any = true;
  public arrFaq : any = [] ;
  constructor(public toastr: ToastrService,private data : DataService,private router : Router,private http : HttpClient,private route: ActivatedRoute) { }

  ngOnInit() {
    this.key = this.route.snapshot.paramMap.get('id');
    this.getFaq();
    this.data.User.subscribe(userData => this.user = userData);
  }
  getFaq(){
    this.http.post('grants/faqs/list',{'grant_id' : this.key }).subscribe(response => {
    if(response['status'].code == "200"){
      this.textLodaing = false;
      this.arrFaq = response['result'];
    }
  });
  }
}
